import styles from './navbar.module.css';
import Link from 'next/link';
import {user, root_path} from '../lib/cmd'

export default function Navbar({ next_path }) {
    next_path = next_path ? next_path : "";
    const path = root_path + next_path;
    return (
        <header id={styles.header}>
            <div className="userpath">
                <span className="user">{ user }</span>:<span className="path">{ path }</span>
            </div>
            <nav id={styles.navbar}>
                <ul id={styles.ulnav}>
                    <li className={styles.linav}>              
                        <Link href={`/`}>
                            <a className={styles.anav}>Accueil</a>
                        </Link>
                    </li>
                    <li className={styles.linav}>             
                        <Link href={`/blog`}>
                            <a className={styles.anav}>Blog</a>
                        </Link>
                    </li>
                    <li className={styles.linav}>             
                        <Link href={`/cv`}>
                            <a className={styles.anav}>CV</a>
                        </Link>
                    </li>
                    <li className={styles.linav}>             
                        <Link href={`/cours`}>
                            <a className={styles.anav}>Cours</a>
                        </Link>
                    </li>
                    <li className={styles.linav}>             
                        <Link href={`/contact`}>
                            <a className={styles.anav}>Contact</a>
                        </Link>
                    </li>
                </ul>
            </nav>
        </header>
    );
}