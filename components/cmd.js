import { user, root_path, get_cmd } from "../lib/cmd";
import styles from "./cmd.module.css";
import Link from "next/link";

export default function Cmdline({ next_path, cmd }) {
    next_path = next_path ? next_path : "";
    const path = root_path + next_path;
    return (
        <div className="userpath">
            <span className="user">{user}</span>:<span className="path">{path}</span>{cmd ? "$ " + cmd : get_cmd(next_path)}
        </div>
    );
}

export function Endline({ next_path }) {
    next_path = next_path ? next_path : "";
    const path = root_path + next_path;
    console.log(next_path);
    return (
        <div className="userpath">
            <span className="user">{user}</span>:<span className="path">{path}</span>
            <span className={styles.line}>$<span className={styles.cmd} id={styles.cmdstart}>|</span>
                <Link href="/">
                    <a className={styles.cd}>cd {root_path}</a>
                </Link>
                <span className={styles.cmd} id={styles.cmdend}>|</span>
            </span>
        </div>
    );
}

export function Homeline() {
    return (
        <div className="userpath">
            <span className="user">{user}</span>:<span className="path">{root_path}</span><span className={styles.line}>$<span className={styles.cmd} id={styles.cmdstart}>|</span>
                <span className={styles.cd}>
                    echo Hello World !
                    <br />
                    <pre id={styles.helloworld}>
                        #  __/\\\________/\\\_________________/\\\\\\_____/\\\\\\_____________________________/\\\______________/\\\______________________________/\\\\\\____________/\\\________________/\\\____........<br />
                        #  ._\/\\\_______\/\\\________________\////\\\____\////\\\____________________________\/\\\_____________\/\\\_____________________________\////\\\___________\/\\\______________/\\\\\\\__.......<br />
                        #  .._\/\\\_______\/\\\___________________\/\\\_______\/\\\____________________________\/\\\_____________\/\\\________________________________\/\\\___________\/\\\_____________/\\\\\\\\\_......<br />
                        #  ..._\/\\\\\\\\\\\\\\\_____/\\\\\\\\_____\/\\\_______\/\\\________/\\\\\______________\//\\\____/\\\____/\\\______/\\\\\_____/\\/\\\\\\\_____\/\\\___________\/\\\____________\//\\\\\\\__.....<br />
                        #  ...._\/\\\/////////\\\___/\\\/////\\\____\/\\\_______\/\\\______/\\\///\\\_____________\//\\\__/\\\\\__/\\\_____/\\\///\\\__\/\\\/////\\\____\/\\\______/\\\\\\\\\_____________\//\\\\\___....<br />
                        #  ....._\/\\\_______\/\\\__/\\\\\\\\\\\_____\/\\\_______\/\\\_____/\\\__\//\\\_____________\//\\\/\\\/\\\/\\\_____/\\\__\//\\\_\/\\\___\///_____\/\\\_____/\\\////\\\______________\//\\\____...<br />
                        #  ......_\/\\\_______\/\\\_\//\\///////______\/\\\_______\/\\\____\//\\\__/\\\_______________\//\\\\\\//\\\\\_____\//\\\__/\\\__\/\\\____________\/\\\____\/\\\__\/\\\_______________\///_____..<br />
                        #  ......._\/\\\_______\/\\\__\//\\\\\\\\\\__/\\\\\\\\\__/\\\\\\\\\__\///\\\\\/_________________\//\\\__\//\\\_______\///\\\\\/___\/\\\__________/\\\\\\\\\_\//\\\\\\\/\\_______________/\\\____.<br />
                        #  ........_\///________\///____\//////////__\/////////__\/////////_____\/////____________________\///____\///__________\/////_____\///__________\/////////___\///////\//_______________\///_____<br />
                    </pre>
                    <span className="user">{user}</span>:<span className="path">{root_path}</span></span><span className={styles.cmd} id={styles.cmdend}>|</span>
            </span>
        </div>
    );
}