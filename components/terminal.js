export default function Terminal() {
    return (
        <div>
            <p>
                Bienvenue sur mon site web. <br />
                Dans le répertoire blog, je partage des projets que j'ai réalisés, des articles sur des technologies que j'ai découvertes.
                On peut le voir comme un carnet de notes. Dans le dossier cv, vous pourrez obtenir plus d'informations me concernant. Enfin, le dernier répertoire permet de me contacter
                ou de suivre mes activités.
            </p>
            <p>
                Le design de ce site est celui d'un terminal. Cette référence à l'informatique permet d'alléger le design. J'ai notamment converti les miniatures des images
                pour le blog en ASCII Art. Le poids par rapport à une image est plus faible. Pour réaliser ce site web, j'ai utilisé l'aspect générateur de sites statiques de Next js. 
                Ce type de génération permet de ne pas dépendre d'un langage côté serveur. L'aspect statique permet d'avoir un chargement plus rapide des pages. 
                En outre des performances obtenues, c'est aussi une démarche d'écoconception web. J'ai essayé de suivre au mieux les pratiques de ce livre : <a href="https://www.eyrolles.com/Informatique/Livre/ecoconception-web-les-115-bonnes-pratiques-9782416006272/" target="__blanck">Ecoconception web : les 115 bonnes pratiques</a>. 
                Néanmoins, j'ai réalisé quelques animations en CSS car l'écoconception est un équillibre entre écologie et experience utilisateur. Conscient des enjeux de notre siècle, <a href="https://vimeo.com/32564879" target="__blanck">soyons le colibri qui éteint la forêt</a>.
            </p>
        </div>
    );
}
