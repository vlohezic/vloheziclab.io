export const user = 'vlohezic@gitlab.io';
export let root_path = '~/';

export function get_cmd(next_path) {
    switch (next_path) {
        case "cv": 
            return "$ cat photo.txt";
        case "blog": 
            return "$ ls";
        case "contact":
            return "$ ./display_contact.sh";
        case "cours":
            return "$ ./display_enseirb_cours.sh";
        default:
            return "$ help";
    }
}