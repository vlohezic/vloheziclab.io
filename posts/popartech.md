---
title: 'PopArTech'
date: '2022-06-20'
thumbnail: "<pre>
    ...........................................Y.........Y.????????.....................................<br />
    ............................?????..........5G#&&&&&&&&#B##GP55Y..????...............................<br />
    .........................??..5P5Y.??.......?.YPB&@@@@@@@@@@@@@@&BGPY..???...........................<br />
    .....................???.5GB&@@@@#PY.??......???.5G&@@#GGB#&@@@@@@@@&#G5Y???........................<br />
    ..................???YPB&@@@@@@&#GG##G5.???.....???.5G##P.7!7?YPB&@@@@@@&BPY.??.....................<br />
    ................??.PB&@@@@@&B5.?7!!7YG##G5..??.....???.YP##GY!^^^!?YG&@@@@@@#PY???..................<br />
    ..............?.5G&@@@@@#P.7!!!!!!!!!!7.P##BPY.??......??.YP##B57~^^^~?5B@@@@@@BP.??................<br />
    ............?.5#@@@@@&G.7!!!!!!!!!!!!!!!!7?5B##GY.??......???Y5B#BP?~^^^^7YB@@@@@&G.??..............<br />
    ..........??Y#@@@@@#Y7!!!!!!!!!!!!!!!!!!!!!!!?5B##G5.???.....???.5B##G.!^^^^7P&@@@@&GY??............<br />
    .........?YB@@@@@B.7!!!!!!!!!!!!!!!!!!!!!!!!!!!!7YG##B5.???.....???.YG##GY!^^^~Y#@@@@&P.?...........<br />
    .......?.P&@@@@#Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!7.P##B5Y??.......??.YPB#B57^:~Y#@@@@&5.?.........<br />
    ......?.B@@@@&57!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?5B&B5?.........??..5B#BP?~~5&@@@@G.?........<br />
    .....?5#@@@@B?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!7B&G..............???.5G##P..B@@@@#Y?.......<br />
    ....?P&@@@@P7!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Y&&5??.....??.???......??.YP##B@@@@@&Y?......<br />
    ...?5&@@@@5!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!7P@BY?......?YB&BPY.??......??.YPB&@@@@&5?.....<br />
    ..?5&@@@@5!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.#&P.?.....??5&&PG###PY.???.....???.5B#@@&Y?....<br />
    .?.&@@@@P!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!75&#Y?......?.G&BYYYY5PB##G5.???.....???.YP##.?...<br />
    ..B@@@@B7!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?B@G.?......?Y#&PYYYYYYYYYPB##B5.???......??.Y.....<br />
    55@@@@&?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Y&#5?......?.P&#5YYYYYYYYYYYYY5G##B5Y.??......?.....<br />
    PB@@@@P!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!7P@B.?......?.B&GYYYYYYYYYYYYYYYYYY5G###PY.??.........<br />
    G&@@@&.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.#&P.?.....??5#&PYYYYYYYYYYYYYYYYYYYYYY5PB##GY.???.....<br />
    .@@@@#7!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!75&#Y?......?.G&BYYYYYYYYYYYYYYYYYYYYYYYYYYYYPB##G5.???..<br />
    ?@@@@G!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?B@G.?.....??Y#&PYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYP&@&B5Y..<br />
    Y&@@@5!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Y&#5.?.....?.P&#5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY#@@@@P?.<br />
    .YPB&#5?7!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!7P@B.?......?YB@BYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY5&@@@&.?.<br />
    .??.YPB##P.7!!!!!!!!!!!!!!!!!!!!!!!!!!.#&P.?.....??5&&PYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYP@@@@&.?.<br />
    ....???.5B##GY7!!!!!!!!!!!!!!!!!!!!!!5&#Y?......?.G@B5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYG@@@@B...<br />
    ........??.5G##GY?!!!!!!!!!!!!!!!!!?B&G.?......?Y#&GYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY&@@@@5?..<br />
    ...??......??.YP#&B5?7!!!!!!!!!!!!Y&&5??.....?.P&#5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYG@@@@B....<br />
    ..P5Y???......??.YPB##P.7!!!!!!!7P@B.?......?.B@BYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY5&@@@@5?...<br />
    .?5&&#PY.??......??..PB##GY7!!!.B&P??.....??5&&PYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY#@@@@G.....<br />
    ..?P@@@@#PY.??......???.5G##GY5&#Y?......?.G&#5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY#@@@@B.?....<br />
    ...?P@@@@@@#G5.???......??.YGB&P.?......?Y#&GYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY5#@@@@#Y?.....<br />
    .....G@@@@@B5G#BPY???......???..?.....?.P&#5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY5#@@@@B.?......<br />
    ......5&@@@@G!!YG#BPY.??.............?YB@GYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYG&@@@@G.?.......<br />
    ......?Y#@@@@&Y^^!.P##G5.??..........?5#&BP5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY5#@@@@&P??........<br />
    .......?.P&@@@@B.^:^~?5B#B5.???.......?.5PB##G5YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY5G&@@@@BY?..........<br />
    .........?YB&@@@@B.~^^^~75B#BPY.??......??.YPB##B5YYYYYYYYYYYYYYYYYYYYYYYYYYYY5G&@@@@#P.?...........<br />
    ..........??Y#@@@@@#5!^^^^^!YG#BPY.??......??.Y5B##BPYYYYYYYYYYYYYYYYYYYYYYY5B&@@@@&G.?.............<br />
    ............?.5B&@@@@@BY!^^^^^!.P##G5.???.....???.5G#&BP5YYYYYYYYYYYYYYYY5G#@@@@@&GY?...............<br />
    ..............??.P#@@@@@&BY7~^^^^~?PB#BP.???......??.YP###G5YYYYYYYYYY5G#&@@@@@BPY??................<br />
    ................??.YG#@@@@@@#GY?!^^^^7YB#BPY.??......??.YPB##G5YYY5PB&@@@@@@#GY???..................<br />
    ...................??.YP#&@@@@@@&BG5.7~~7YG#BPY.??......??.Y5G###&@@@@@@&#PY.??.....................<br />
    ......................??.Y5G#&@@@@@@@@&#BGPB&@@#G5.??......???.5P#&@&#G5Y.??........................<br />
    .........................???..YPG##&@@@@@@@@@@@@@@&BP..........??..YY.???...........................<br />
    .............................????...Y5PGB###&########BP...........???...............................<br />
    ...................................??????.?.........................................................<br />
    
    
</pre>"
---

PopArTech est une association de makers à Lorient au collège Anita Conti. En tant que membre de cette association, je me suis occupé du site web pendant que j'étais étudiant au lycée et en prépa. 

Aujourd'hui le site web repose sur [le générateur de sites statiques Pelican](https://blog.getpelican.com/). Celui-ci utilise Python. Pour la partie graphique, le [framework Materialize](https://materializecss.com/) est exploité. Le site de PopArTech est accessible avec ce [lien](https://popartech.com/). Le code source est disponible [ici](https://gitlab.com/iRoboTechArt/irobotechart).


J'ai aussi écrit des articles sur ce site web sur diverses thématiques qui me passionnent : 
- [OpenScad](https://openscad.org/) (modélisation 3D à l'aide de script)
- [Piskel](https://www.piskelapp.com/) (réalisation de Pixel Art)
- [GDevelop](https://gdevelop.io/fr-fr) (création de Jeux vidéos )
- [Montage d'un robot nommé Maqueen](https://www.gotronic.fr/art-chassis-micro-maqueen-28705.htm)
- [GB Studio](https://www.gbstudio.dev/) (création de jeux vidéos GameBoy)
- [MagicaVoxel](https://ephtracy.github.io/) (voxel Art)

Enfin, j'ai aussi proposé des formations aux membres pour créer leur site web en utilisant Pélican. Elles se découpaient en trois parties :
- Introduction aux bases de Python
- Présentation de Pélican
- Gitlab Pages
