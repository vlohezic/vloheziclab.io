---
title: 'TIPE WATOR'
date: '2022-06-22'
thumbnail: '<pre>
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
...................G#&@@@&###BG5Y.????........................................................???...<br />
...................B&#&&#B##&&&&&#BP5.???.....................................????...?????....5YY...<br />
...................?5B@&#G#&&###&&&&&&BPY.???....................????????????.Y55Y.?.Y5PPY..?YP5P...<br />
....................??YB&#PG#&##&&###&&&&#PY.?????...?????????....YY555YYY.YG##&&##BBBBB#@5?.??.....<br />
......................??5&&##&&&&#BBBB#&&&&@&#GPYY....YYY5PGB#&#&&&@@@@@@&&&##B5YYPBBYY5YP&.?.......<br />
........................?.#@&&##&&#BGGGB#&&&&&&@@&BBBBB#&@&&#BGGGGGB##&######5.55???P..P..#.?.......<br />
.........................?.G@&##&&BGGGPPG#GBGBGGGPPGBBGPYY..?????????Y&#B###B?.55??YBB55G&BY..??....<br />
..........................??G@##&&&&&#PPPPPPP5Y5PG#GY.????.....???.YP##GGBBBBGGGPPG########&##GY??..<br />
............................?#&&&&&&&&BPPPPP5555B#.???..??????.Y5G####BBGBGPPPGB###BBBBBBBBB###&#5?.<br />
............................?G@&&&&&&&&GPPPP..PPG#PY.....Y5PGB#####BBBB#BB#B#BPGBBBBBBBBBBBBBBGB&@P?<br />
..........................?.G&&&&&&&#BBGP5PPPPPPPB&&######&#BBBBBBB#BGB#B##&&BBB##BBBB#BBBBG5555PG@G<br />
........................?.P&@##&&&&&##&BPPPPPPPPPPGGGGBBBBGGPPBGP55GB#&#G#@#&&##&#BGPB#BBBBBBGBGY5&@<br />
......................?.P&@#BB#&&#GGGGBBBBPPPPPPPPPPBBBBBGP555BG5Y5PG&&Y?Y#?!.B5P###&&&&&###B###B#@G<br />
....................?.PBPYG&&&&&###BPPPPGBPPP55PPPPPPPPPPPY.5PGBBBB#&G.?.?YBY~^..!..5YY5PPGBBB@#GPY?<br />
..................?.PBP.???.&&&&##&&#PPPP5PPPPPPPPPPPP5555PPBBBB###BY??...?.PG5.7?..?????????GP.???.<br />
................??5BP.??????B&&&&&&&&GPPBB#BBGGGGB#####BGGPPB###BPY??.......??.YPPGGPP55555YB#??....<br />
...............?.B#.????..5B&&&&&&&&&&##&&&&&&&&&BGGGGPPGGB#&&P.???............????..YYYY55PGGBY?...<br />
..............?Y&@&##BB##&&&####&&#####&&&&&&#&@&##BPP5.YYYY5B#G.?..................???......5#Y?...<br />
.............?5&&&&&&&&##BBBBBBB##&&##BGGP5Y.P#&###BBBBG5555PGB&#.?..............???.5GGGPPPP5.?....<br />
............?Y&&&&##BBGGGGGBB##&#BG5Y..????YB&&#&&&&&&&BP555PGGB@P?........?????.YPGG5Y???????......<br />
.............#&###BBGPPPPB#&#PY?..???...?.G&&&&&&&&&&&&#BP5PPGPP&P????????.YY5GB&BY.???.............<br />
...........?P@####BGPPGB#&G?~:::~7??????P&&&&&&&&&&&##BGPB##BBB#BY.YY5PPGB#BBGP5G#P?................<br />
............#&&#BGPPPB&#5!^:::^^~~7Y55YG@&&&&&&&&#BPY5###&&&#&BGGPPGPPPBB#&&##G5Y5@5?...............<br />
..........?Y@&#&#G5PB&5~::^!.5PPPPPPP5G@&&&##&&BGP5PGB&&#BG5Y.???????????..Y5GB#BG#Y?...............<br />
...??????.?5@&BBGPPG&.::~?5PY...??????.B&&&&&&&###BGGP5Y.????..............???..YY.?................<br />
...Y5555Y.?5@&&#BGPB#^:7G5Y5555555.....?.YYYYYY...??????........................??..................<br />
.?G#GPP5PGG5&&&&#GPG#^.BPP5Y..5PG#G?....???????.....................................................<br />
.YPGPPP.?.YG#@&&&#GPGY#BY????.PGG#P?................................................................<br />
BPYYPPY.Y5PP5G&@&&#BG&@G5PP5Y.5B#BP?................................................................<br />
YYYBG555YYY555PPGGGGGGGGPPY5PBGGB.?.................................................................<br />
??.P55.?????YGGGGP555YYY5Y???.YP.?..................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />


</pre>'
---

Le TIPE est une épreuve orale lors des concours pour intégerer une école d'ingénieur. J'ai travaillé avec Gabriel Giovannelli. Pour celle-ci, nous avons travaillé sur différentes modélisations permettant  de décrire l'évolution de différentes populations. L'étude se restreint à un modèle proie-prédateur.

Une première modélisation est celle proposée par Malthus. Dans l’équation de cette modélisation aucune composante permet de borner les solutions, donc le nombre de proies augmente sans interruption.

Alfred Lokta et Vito Volterra ont alors travaillé chacun de leur côté sur un modèle permettant de décrire l'évolution des quantités de fourrures de lièvres et lynx achetées par la Compagnie de la Baid D'Hudson. [Cette modélisation](https://fr.wikipedia.org/wiki/%C3%89quations_de_pr%C3%A9dation_de_Lotka-Volterra) décrit de manière plus réaliste le comportement de proies et prédateurs. Avec cette modélisation, on peut notamment observer un phénomène de périodicité dans l'évolution des proies et des prédateurs. 

Une autre approche est possible. La dynamique de population est modélisée informatiquement avec un [automate cellulaire](https://www.futura-sciences.com/tech/definitions/informatique-automate-cellulaire-8909/).

En 1984, dans la revue Scientific American, A; K. Dewdney propose de créer une planète dans laquelle on fait évoluer des requins et des poissons. Le Wator est un monde en forme de Tor comme son nom l’indique. Dans ce celui-ci, le temps prend la forme de chronons. La modélisation est donc discrète. A chaque chronon, on choisit une case aléatoire et si celle-ci est remplie, on déplace le poisson ou le requin selon un comportement précis. La modélisation du Wa-Tor est détaillée [ici](https://en.wikipedia.org/wiki/Wa-Tor).

Dans le cadre de notre TIPE, notre problématique était : **quelle influence peut avoir l'homme sur un système proie prédateur ?**

Nous avons mené trois études :
- [une première sur la pêche](https://drive.google.com/file/d/1qQMw_Q47wkwNhOkhAszcAHGKm4CKnt0V/view?usp=sharing) (étude que j'ai présentée)
- [une seconde sur l'urbanisation](https://drive.google.com/file/d/1Z533_pt8l1Cbbb6ZNxr6D1tjKWFIlqxx/view?usp=sharing) (étude présentée par Gabriel Giovannelli)
- [une denière étude sur les écoponts](https://drive.google.com/file/d/1oJ53F3hmRxCaMpgPPSuxWxvi4tXtepAI/view?usp=sharing) (étude non présentée)


Le diaporama de ma présentation est disponible [ici](https://docs.google.com/presentation/d/1W7GSCfO0eWrtKu3Ev5j9tJtCFOhej-oDkCf7WvcZar0/edit?usp=sharing).