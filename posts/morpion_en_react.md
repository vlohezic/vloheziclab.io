---
title: 'Morpion en React'
date: '2022-06-30'
thumbnail: "<pre>....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
    ........^^:::::::::::::::::::::::::~^:::::::::::::::::::::::::~!7777777777777777777777777!..........<br />
      ........~:                         ~:                         ~Y5555555555555555555555555J..........<br />
      ........~:                         ~:                         ~YYYYYYYYYYYYYYYYYYYYYYYYYYJ..........<br />
      ........~:                         ~:                         ~YYYYYYYYYYYYYYY5YYYYYYYYYYJ..........<br />
      ........~:      :5BP:   7BG!       ~:                         ~YYYYYYYJ^:^J5YY7::7YYYYYYYJ..........<br />
      ........~:       :5@&!.5@#~        ~:                         ~YYYYYYYYJ^ .?Y^ .?YYYYYYYYJ..........<br />
      ........~:         !#@#@J.         ~:                         ~YYYYYYYYY57. . ~Y5YYYYYYYYJ..........<br />
      ........~:          5@@#^          ~:                         ~YYYYYYYYY5Y~  .?5YYYYYYYYYJ..........<br />
      ........~:        ^B@57&@J         ~:                         ~YYYYYYYY5?: ~7. ~Y5YYYYYYYJ..........<br />
      ........~:      .Y@@7  :G@#~       ~:                         ~YYYYYYYY~  7Y5J: .?YYYYYYYJ..........<br />
      ........~:      ~YJ^     7YJ:      ~:                         ~YYYYYYY?~!J5YYYY7~!JYYYYYYJ..........<br />
      ........~:                         ~:                         ~YYYYYYY5555YYYYY555YYYYYY5J..........<br />
      ........~^.:::::::::::::::::::::::.!^::::::::::::::::::::::::.!JYJJJJJJJJJJJJJJJJJJJJJJJY?..........<br />
      ........~^.........................~YYYYYYYYYYYYYYYYYYYYYYYYYY?:.........................~:.........<br />
      ........~:                         ~YYYYYYYYYYYYYYYYYYYYYYYYYYJ.                         ~:.........<br />
      ........~:                         ~YYYYYYYY55YYYYYY55YYYYYYYYJ.                         ~:.........<br />
      ........~:         ^!7?7~:         ~YYYYYYYJ77?5YY5Y77?YYYYYYYJ.         ^!7?7~:         ~:.........<br />
      ........~:      .J#&BPPG&@G!       ~YYYYYYYY!  !Y5?: ^JYYYYYYYJ.      .J#&BPPG&@G!       ~:.........<br />
      ........~:     .G@#^    .J@@?      ~YYYYYYYY5J: ^! .7YYYYYYYYYJ.     .G@#^    .J@@J      ~:.........<br />
      ........~:     !@@!       G@#.     ~YYYYYYYYY55~  .J5YYYYYYYYYJ.     !@@!       P@#.     ~:.........<br />
      ........~:     !@@?       B@B      ~YYYYYYYYYY7..: ^J5YYYYYYYYJ.     ~@@?       B@B      ~:.........<br />
      ........~:      Y@@Y^..:!G@#^      ~YYYYYYY5J^ :JY~ .7YYYYYYYYJ.      Y@@Y^..:!G@#^      ~:.........<br />
      ........~:       ~5###B##G7.       ~YYYYYYY?: ~Y5Y5?..~YYYYYYYJ.       ~5B##B##G7.       ~:.........<br />
      ........~:          .::.           ~YYYYYYYYJYYYYYY5YYJYYYYYYYJ.          .:::           ~:.........<br />
      ........~:                         ~Y5555555555555555555555555J.                         ~:.........<br />
      ........~777777777777777777777777777!!!!!!!!!!!!!!!!!!!!!!!!!!7^:::::::::::::::::::::::::!:.........<br />
      ........~Y5555555555555555555555555J.                         ~:                         ~:.........<br />
      ........~YYYYYYYYYYYYYYYYYYYYYYYYYYJ.                         ~:                         ~:.........<br />
      ........~YYYYYYYYYYYYYYY5YYYYYYYYYYJ.                         ~:                         ~:.........<br />
      ........~YYYYYYYJ^.^J5YY7::7YYYYYYYJ.                         ~:       .7PBBBBGY!        ~:.........<br />
      ........~YYYYYYYYJ^ .?Y^ .?YYYYYYYYJ.                         ~:      7&@P7^^~J&@G:      ~:.........<br />
      ........~YYYYYYYYY5?. . !Y5YYYYYYYYJ.                         ~:     ^@@J      :#@G      ~:.........<br />
      ........~YYYYYYYYY5Y~  .?5YYYYYYYYYJ.                         ~:     7@@!       P@&.     ~:.........<br />
      ........~YYYYYYYY5?: ~7. ~Y5YYYYYYYJ.                         ~:     :#@G:     !@@Y      ~:.........<br />
      ........~YYYYYYYY~  755J: .?YYYYYYYJ.                         ~:      ^P@&PYJYB@#?       ~:.........<br />
      ........~YYYYYYY?!!J5YYYY7!!JYYYYYYJ.                         ~:        :!JYYY?~.        ~:.........<br />
      ........~YYYYYYY5555YYYYY555YYYYYY5J.                         ~:                         ~:.........<br />
      ........~JJJJJJJJJJJJJJJJJJJJJJJJJJ?^.:::::::::::::::::::::::.~^.:::::::::::::::::::::::.~:.........<br />
      ....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br /> </pre>"
---

Pour développer mes connaissances en [React](https://fr.reactjs.org/), j'ai réalisé un morpion. React est une bibliothèque Javascript pour créer des interfaces utilisateurs. J'ai donc suivi [leur tutoriel](https://fr.reactjs.org/tutorial/tutorial.html). Afin d'aller plus loin, il propose quelques fonctionnalités à ajouter. J'ai donc complété le morpion : 
- Afficher l’emplacement de chaque coup dans l’historique de tours, au format (colonne, ligne). ✅
- Mettre le tour affiché en gras dans l’historique. ✅
- Réécrire Board pour utiliser deux boucles afin d’afficher le plateau, plutôt que de coder tout ça en dur. ✅
- Afficher un bouton de bascule qui permette de trier les tours par ordre chronologique, ou du plus récent au plus ancien. ✅
- Quand quelqu’un gagne, mettre en exergue les trois cases qui ont permis la victoire. ✅
- Quand personne ne gagne, afficher un message indiquant le match nul. ✅

Le morpion dispose d'un historique qui permet de revenir un ou plusieurs coups en arrière. Le développement de cette fonctionnalité a été l'occasion pour moi de mettre en pratique les connaissances acquises dans le cours de programmation fonctionnelle. 

Le morpion est disponible à l'adresse suivante : [https://vlohezic.gitlab.io/react_morpion/](https://vlohezic.gitlab.io/react_morpion/).    