import Head from 'next/head';
import Layout, { user } from '../components/layout';
import utilStyles from '../styles/utils.module.css';
import Terminal from '../components/terminal';
import { Homeline } from '../components/cmd';

export default function Home() {
  return (
    <Layout home={ true } next_path="">
      <Head>
        <title>{user}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <Terminal />
      </section>
      <Homeline />
      <p>{user}</p>
    </Layout>
  );
}