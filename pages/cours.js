import Layout from "../components/layout";
import Cmdline from "../components/cmd";
import asciiStyles from "../styles/ascii.module.css";
import cvStyles from '../styles/cv.module.css';
import Link from "next/link";

export default function Cours() {
    const next_path = "cours";
    return (
        <Layout next_path={next_path}>
            <br/>
            <br/>
            <div className={cvStyles.grid2col}>
                <div><b>Conception formelle des logiciels</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div></div>
                <div><b>Méthodologie et outils logiciels</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div>Acquérir des compétences sur les méthodes et les technologies les plus récentes pour aider au développement d'applications</div>
                <div><b>Développement d'applications mobiles</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div></div>
                <div><b>Applications concurrentes et distribuées</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div>Bases et concepts utilisés dans les applications modernes</div>
                <div><b>Gestion et analyse de masse de données - Big Data</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div></div>
                <div><b>Persistance et bases de données</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div>Connaitre l’ensemble des outils pour porter un regard critique et faire le choix le plus pertinent</div>
                <div><b>Spécification et preuve formelle de programmes</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div></div>
                <div><b>Conception orientée objet</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div>Architectures logicielles, composants, dépendances, substitutivité, couplage et cohésion, responsabilités, séparation des rôles, ségrégation, concept de l'ouvert/fermé</div>
                <div><b>Urbanisation et Architecture des Systèmes d'Information </b><br /><span className={cvStyles.description}>Semestre 9</span></div><div>Méthodologie et outils techniques</div>
                <div><b>Conduite de projet</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div>Ensemble d'outils permettant de comprendre, d'appréhender et/ou de gérer un projet dans une organisation donnée</div>
                <div><b>Test du logiciel</b><br /><span className={cvStyles.description}>Semestre 9</span></div><div>Comprendre comment piloter et organiser les tests dans une logique de couverture des exigences et de maitrise des risques produits</div>
                <div><b>Management de projet digital et innovant</b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Concepts pour innover</div>
                <div><b>Parcours entrepreneur</b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Temps pour travailler sur son projet</div>
                <div><b>Securite des systemes d'information</b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Principaux enjeux et techniques de la sécurité des systèmes</div>
                <div><b>Introduction à la robotique</b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Concepts de bases de la modélisation géométrique et cinématique des robots, de la planification de trajectoires sur des robots articulaires et mobiles</div>
                <div><b>Intelligence artificielle</b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Introduction aux grandes problématiques de l'intelligence artifielle, à l'aide de l'étude de trois grandes approches de l'IA : les algorithmes de recherche (dans le cadre des jeux de plateaux) et le calcul d'heuristiques, les algorithmes de raisonnement automatique, dans le cadre d'approches déclaratives de problèmes, et l'apprentissage automatique (restreint aux réseaux de neurones)</div>
                <div><b>Applications TCP/IP </b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Mise en place du modèle TCP/IP avec illustration et utilisation d'applications TCP/IP</div>
                <div><b>Systèmes d'exploitation</b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Processus et exécution, Gestion mémoire, Concurrence et synchronisation, Gestion du temps, Système de fichiers, Entrées-sorties, Virtualisation</div>
                <div><b>Calculabilité et complexité</b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Machine de Turing, MT Universelle, existence de fonctions non calculables, exemples de problèmes indécidables, principe de réduction, classes de complexité</div>
                <div><b>Cryptologie</b><br /><span className={cvStyles.description}>Semestre 8</span></div><div>Initiation à une grande variété de protocoles et étude de la sécurité de certains d'entre eux</div>
                <div><b>Architecture des réseaux TCP/IP</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Protocoles TCP/IP, couche réseau : IP, ICMP, couche transport : TCP/IP</div>
                <div><b>Systèmes de Gestion de Bases de Données</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Acquérir une compétence dans le domaine des bases de données et celui de leur mise en oeuvre</div>
                <div><b>Introduction à l'informatique quantique</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Algorithmes quantiques : algorithmes de Deutsch et de Simon, algorithme de factorisation de Shor</div>
                <div><b>Programmation C++</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Présente les bases de la programmation C++, l'accent est mis sur les outils et techniques liés à la gestion de la mémoire</div>
                <div><b>Programmation système</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Présente les concepts et les techniques permettant de programmer dans un environnement POSIX</div>
                <div><b>Programmation Orientée Objet</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Initiation à la programmation orientée objets avec comme langage de support Java</div>
                <div><b>Génie Logiciel</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Méthodes et les outils modernes pour le développement de logiciels, développement agile</div>
                <div><b>Compilation</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Présentation des techniques et outils standards pour la compilation des langages de programmation</div>
                <div><b>Introduction aux réseaux</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Introduction aux réseaux avec présentation des modèles OSI et TCP/IP</div>
                <div><b>Programmation impérative 2 et développement logiciel</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Approfondissement des mecanismes de gestion mémoire, de la compilation et la prise en main des outils de développement logiciel pour l'écriture de projets maintenable, portables et robustes</div>
                <div><b>Programmation fonctionnelle</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Etude du paradigme avec JavaScript</div>
                <div><b>Recherche Opérationnelle</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Optimisation mathématique</div>
                <div><b>Algorithmique numérique</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Décrit un ensemble de méthodes et d'algorithmes adaptés à la modélisation de problèmes numériques</div>
                <div><b>Automates finis et applications</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Automates finis, langages réguliers, expressions régulières, équivalence de ces trois formalismes, non-déterminisme, automate minimal, lemme de l'étoile</div>
                <div><b>Algorithmique des graphes</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Introduction aux graphes</div>
                <div><b>Programmation Impérative 1</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Apprendre les bases de la programmation impérative par l'étude de la syntaxe et la sémantique du langage C</div>
                <div><b>Structure des ordinateurs</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Architecture des ordinateurs, pour les processeurs généraliste</div>
                <div><b>Environnement de Travail</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Programmation shell permettant d'automatiser des tâches d'administration système.</div>
                <div><b>Logique et Preuve</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Construire un raisonnement formel, ainsi que de prouver la terminaison et la correction des algorithmes</div>
                <div><b>Probabilités et statistiques</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Introduction: probabilité sur un espace fini, Variables aléatoires discrètes, Variables aléatoires à densité, Convergences et théorèmes limites</div>
                <div><b>Traitement de l'information</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div><a href="https://fr.wikipedia.org/wiki/Analyse_en_composantes_principales">ACP</a>, <a href="https://fr.wikipedia.org/wiki/Analyse_factorielle_des_correspondances">AFC</a>, introduction à l'apprentissage automatique</div>
                <div><b>Structures Arborescentes</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Rappelle quelques structures mathématiques usuelles (ensemble, séquence et arbre), introduit la notion de type abstrait</div>
                <div><b>Initiation à l'algorithmique</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Initiation à la résolution de problèmes simples au moyen de l'algorithmique</div>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </Layout>
    );
}
