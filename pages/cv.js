import Layout from "../components/layout";
import Cmdline from "../components/cmd";
import asciiStyles from "../styles/ascii.module.css";
import cvStyles from '../styles/cv.module.css';
import Link from "next/link";

export default function Cv() {
    const next_path = "cv";
    return (
        <Layout next_path={next_path}>
            <pre className={asciiStyles.photo_ascii}>
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ...............................................!7?JJYYYJJJ?7!.......................................<br />
                .........................................!7JY5PGGPPGGGGP55PP55YJ7!..................................<br />
                ......................................7Y5PPGBBBGGBBBGGGPPGGGGGGGGP5Y7...............................<br />
                ....................................?5PGGBBBGGGGBBBBBBBBB########BGGP5J.............................<br />
                ..................................7J5PPGBBBGBBBBGGGGGGGBBBBBBB##BBBGGPPY............................<br />
                .................................75PPGBBBBBB#BGGGBBBB#B######BBBBBBBGGPPY!..........................<br />
                ................................7YGGB##BBBB#BBBBBBBBBGGGGGGGGGBBGGGGGGGP55?.........................<br />
                ................................YB##BGP555YYYYYYYY555YYJJJJJJJYYY555PGBBP5557.......................<br />
                ...............................JPBBG5J????777777777??77777777777????JY5GGPPP5!......................<br />
                ..............................J5PGPY???7777777777777777777777777?7?????Y5GPP5J......................<br />
                .............................755PPJ?????7777777777!!!!777777777????????JJY5P5J......................<br />
                .............................?55YJ??????777777777777777777777777??????JJJYYY5?......................<br />
                .............................7YYJ?????????77777777777777777777??????JJJJYYYYY?......................<br />
                ..............................JJJ??????????7777777777777777777?????JJJJYYYYYY7......................<br />
                ..............................7J????????7777777777777777777777??????JJJYYJJY57......................<br />
                ...............................J????????777777!!!!!!!!!!77777????JJJJJJJJJJJY!......................<br />
                ...............................7?7??JY55555JJ??7777777777??JY55PGBGGPGG5YJ?JJ.......................<br />
                ...............................!77J5PPPPPGBBBGPYJ??77???J5PB###BBGPPP5555Y?J?!......................<br />
                ..............................??77?JJY55GBB#GGPP5J?77?JYPGGGGPG#B#BPGG5YYYJJJYJ.....................<br />
                ..............................J?77??YP5JPB#GJJYYYJ??7?J55555J?JGBG5YPP5YYYJ?Y5Y.....................<br />
                ..............................??77??JJYJJYYYJJJJYJ???JY55YYYJJJYYYYYYYJJJJJ?JYJ.....................<br />
                ...............................777?????JJ?????JJJJ???JYYYYYJJJJJJ???JJJJJJJJYJ......................<br />
                ...............................!777?????????????JJ???JYYYYYJ????????JJJJJJJJY7......................<br />
                ................................!777777777777??JJ?????JYY5YJ?????????JJJJJJJJ!......................<br />
                ................................!7777777777777????777?JJYY5J?77??????JJJJJJYJ!......................<br />
                ..................................777777777777?JJJ?77?J55Y5Y??7?????JJJJJJ?7!.......................<br />
                ..................................!7777777777??JYYJJYYPPP5YJJ?????JJJJJJJJ?!!.......................<br />
                ...................................7??77???????JJJJJYYYYYYYJJJJJJJJJJJJJJJ?!........................<br />
                ...................................7??????JJJ??????J???JJJYYYY55YYYYYYYYYJ?!........................<br />
                ....................................?????JYYYYYYYYYYYY5555PPPPP5YYYYYYYYYJ7!........................<br />
                ....................................!????????JYYYYJJJYYYY5555YYJJYYYYYYYY?!.........................<br />
                .....................................!??????????JJJJYYYYYYYYJJJJYYYYYYYY?!!.........................<br />
                ......................................!?JJJ?????????JJJ???JJJJJJYYYY55J7!!..........................<br />
                ........................................JYJJJ?????????????JJJJJYY55PPY77!!..........................<br />
                ........................................?JYYJJJJ????7??JJJJYYYY5PGPP5Y77!!..........................<br />
                .....................................!!7???JY5YYJJ????JJJY55PPGGP55YYY?7!!..........................<br />
                ..................................!7YPJJ?????JY55555555PPGGGGPP5YYYYYYYJ777!........................<br />
                ............................!5PGGBBPP5JJ?????????JJJJJYYYYYYYYYYYYYYY55YY5GG?77!....................<br />
                .....................7JY5PGG#&####BPPPJ?J???777?????777??JJYYYYJYYYY55555Y5BBB##B5?!!...............<br />
                ..............7777?YB#B#&&#&##B####GPG5J???777777??????JJJJJJJJJJYYY555555G###&&&&&#BG5Y?7!.........<br />
                ........?JY5GB######BB#&&#####B##&#BGGG5J??777777777??????J????JJYYY55555GB##&&&&&&&&&&&##BG55J!....<br />
                ......JGB#####BBBBBB##&&#########&##BGGGPJ?7777777777777???????JJYYY555PG##&&&&&&&&&&&&&######BG5?!.<br />
                .?Y5PGB#####BBBB#####&&##########&&###BGGG5J?77777777777777????JJYYY55PG#&&&&&&&&&&&&&&&########BBGP<br />
                GGGGBBB###########&&&&###########&&&###BBGGGPYJ?77777777777???JY5PPPGB##&&&&&&&&&&&&&&&&&########BB#<br />
                GGGGGB##########&&&&&############&&&&##&##BBBBBGGPP55555555PGGBB####&&&@&&&&&&&&&&&&&&&&&&&#######BB<br />
                BBBBBB########&&&##&&#######&####&&&&&#&&########B###############&&&&&&&&&&&&&&&&&&&&&&&&&####&####B<br />
                B##BB######&#&&&###########&#####&&&&&&#&&&###&##########&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&######&###<br />
                B##BB###&#&&###############&#####&&####&##&&###&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&######&###<br />
                ##BBB##&&&#&#############B#&#####&&########&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&###########<br />
                ####B##&&&###############B#&######&#########&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&#####&&#&##<br />
                ####B#&&&####################################&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&#&&&&&&###########<br />
            </pre>
            <Cmdline next_path={next_path} cmd="echo $nom" />
            Victor Lohézic
            <Cmdline next_path={next_path} cmd="cat formation.txt" />
            <div className={cvStyles.grid2col}>
                <div><b>2021-2024</b></div><div> Ecole d'ingénieur ENSEIRB-MATMECA (Bordeaux) en filière informatique</div>
                <div><b>2019-2021</b></div><div>  Prépa MPSI & MP au Lycée Dupuy de Lôme à Lorient</div>
                <div><b>2019</b></div><div>  BAC général en filière scientifique option sciences de l'ingénieur</div>
            </div>
            <Cmdline next_path={next_path} cmd="cat passion.txt" />
            <div className={cvStyles.grid2col}>
                <div><b>Etudiant Entrepreneur </b><br /><span className={cvStyles.description}>Statut national</span></div><div>Création d'un site web sur l'animation</div>
                <div><b>Eirlab </b><br /><span className={cvStyles.description}>FabLab au sein de l'ENSEIRB-MATMECA à Bordeaux</span></div><div>Découpeuse laser</div>
                <div><b>Popartech </b><br /><span className={cvStyles.description}>association de makers au sein du collège Anita Conti</span></div><div>Réalisation et rédaction du site web</div>
                <div><b>Plus Grand Groupe de Rock du Monde </b><br /><span className={cvStyles.description}>Lorient</span></div><div>Tuba</div>
            </div>
            <br />
            <Cmdline next_path={next_path} cmd="ls experiences professionnelles" />
            <div className={cvStyles.flexcol}>
                <Link href={"/cv"}>
                    <div className={cvStyles.box}>
                        Développement d'un jeu éducatif sur la santé 4.0 avec le moteur de jeux Unity selon le paradigme de programmation orientée objet en C#. 
                        Développement d'une API avec le framework Django API Rest (python) pour permettre le suivi de la progression des élèves par les professeurs et la mise en place d'un classement pour motiver les joueurs.
                        </div>
                </Link>
                <Link href={"/posts/robotont"}>
                    <div className={cvStyles.box}>Montage d'un robot et exploitation sous ROS</div>
                </Link>
            </div>
            <Cmdline next_path={next_path} cmd="ls projets" />
            <div className={cvStyles.flexcol}>
                <Link href={"https://flippanim.com/"}>
                    <div className={cvStyles.box}>Développement d'un site web pour apprendre l'animation avec Django et d'un jeu vidéo avec le moteur de jeu Godot</div>
                </Link>
            </div>
            <div className={cvStyles.flexcol}>
                <Link href={"https://zestedesavoir.com/"}>
                    <div className={cvStyles.box}>Chef de Projet du PFA (Projet au Fil de l'Année) ZesteDeSavoir</div>
                </Link>
            </div>
            <div className={cvStyles.flexcol}>
                <Link href={"/posts/wator"}>
                    <div className={cvStyles.box}>TIPE Modélisation pour décrire l'évolution des populations d'orques et de thons rouges</div>
                </Link>
            </div>
            <div className={cvStyles.flexcol}>
                <Link href={"/posts/popartech"}>
                    <div className={cvStyles.box}>Développement et rédaction du site Popartech</div>
                </Link>
            </div>
            <Cmdline next_path={next_path} cmd="cat langages_et_logiciels.txt" />
            <br/>
            <div className={cvStyles.flexcol}>
                <table className={cvStyles.skills}>
                    <tr>
                        <td className={cvStyles.skillsTd}>C / C++</td>
                        <td className={cvStyles.skillsTd}>Python</td>
                        <td className={cvStyles.skillsTd}>Java / C#</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>HTML</td>
                        <td className={cvStyles.skillsTd}>CSS</td>
                        <td className={cvStyles.skillsTd}>JS</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>React</td>
                        <td className={cvStyles.skillsTd}>Django</td>
                        <td className={cvStyles.skillsTd}>NextJS</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>C</td>
                        <td className={cvStyles.skillsTd}>Git / GitLab / GitHub</td>
                        <td className={cvStyles.skillsTd}>ROS</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>Oracle</td>
                        <td className={cvStyles.skillsTd}>PostgreSQL / MySQL</td>
                        <td className={cvStyles.skillsTd}>NoSQL</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>PyQT</td>
                        <td className={cvStyles.skillsTd}>Unity / Godot</td>
                        <td className={cvStyles.skillsTd}>Inckscape</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>TOEIC 885</td>
                        <td className={cvStyles.skillsTd}>Reading C1</td>
                        <td className={cvStyles.skillsTd}>Writing B2</td>
                    </tr>
                </table>
            </div>
            <br/>
        </Layout>
    );
}
