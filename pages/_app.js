import '../styles/global.css';
import '../styles/atom-one-dark.min.css';

export default function App({ Component, pageProps }) {
    return <Component {...pageProps} />;
  }
  