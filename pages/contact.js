import Cmdline from "../components/cmd";
import Layout from "../components/layout";
import styles from "../styles/contact.module.css"

export default function Contact() {
    return (
        <Layout next_path="contact">
            <div id={styles.socialmedia}>
                <div>
                    <a href="https://www.linkedin.com/in/victor-lohezic/" className={styles.alogo} target="_blank">
                        <pre className={styles.logo}>
                            @@@#BGPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPGB#@@@<br />
                            @#P5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555P#@<br />
                            #55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555#<br />
                            G55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555555555PPPPPPPPP555555555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555555PPP5J?777?J5PP5555555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G5555555555555P5?^.       .~JPP55555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G555555555555PY^             ~5P5555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555P5^               ~P5555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555PY                .5P555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555P5:               ^55555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G555555555555PJ.             :YP5555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G5555555555555P5!:         :75P55555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555555PP5J?!~~~!?Y5PP555555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G5555555555555555PPPPPPPPPPP5555555555555555555555555555555555555PPPPP55555555555555555555555555555G<br />
                            G55555555555555PPPPPPPPPPPPPPP555555555PPPPPPPPPPPPPPP55555PPPPPP555555PPPPPP5555555555555555555555G<br />
                            G55555555555555??????????????JP555555P5??????????????5P5PPPYJ7~^:::::::^~!7JY5PP5555555555555555555G<br />
                            G555555555555P5.             ^P555555P5.             YPPY7^.                .:!YPP55555555555555555G<br />
                            G555555555555P5.             ^P555555P5.             YY~                        ~YP5555555555555555G<br />
                            G555555555555P5.             ^P555555P5.             !.                          .JP555555555555555G<br />
                            G555555555555P5.             ^P555555P5.                                           JP55555555555555G<br />
                            G555555555555P5.             ^P555555P5.                                           :5P5555555555555G<br />
                            G555555555555P5.             ^P555555P5.                 :!?JJJJJ7~.                ?P5555555555555G<br />
                            G555555555555P5.             ^P555555P5.               ^JPPPPPPPPPP5!               ^P5555555555555G<br />
                            G555555555555P5.             ^P555555P5.              ^5P5555555555PP!              :5P555555555555G<br />
                            G555555555555P5.             ^P555555P5.              YP555555555555P5.             .5P555555555555G<br />
                            G555555555555P5.             ^P555555P5.             :5P5555555555555P:             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^             .YP555555555555G<br />
                            G555555555555P5.             ^P555555P5.             ^P55555555555555P^              YP555555555555G<br />
                            G555555555555P5:.............~P555555P5:.............~P55555555555555P~.............:YP555555555555G<br />
                            G55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555555PPPPPPPPPPPPPPP555555555PPPPPPPPPPPPPPP5555555555555555PPPPPPPPPPPPPPP55555555555555G<br />
                            G55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555G<br />
                            G55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555G<br />
                            #55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555#<br />
                            @#P5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555P#@<br />
                            @@@#BGPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPGB#@@@<br />
                        </pre>
                    </a>
                </div>
                <div>
                    <a href="https://gitlab.com/vlohezic" className={styles.alogo} target="_blank">
                        <pre className={styles.logo}>

                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~!?JJJ?~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~?JJJ?!~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~!YYYYYY?~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~JYYYYYY!~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~JYYYYYYY7~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~7YYYYYYYJ~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~?YYYYYYYYY~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~YYYYYYYYY?~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~?YYYYYYYYYYJ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~JYYYYYYYYYY7~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~7YYYYYYYYYYYY7~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~7YYYYYYYYYYYY7~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~!YYYYYYYYYYYYYY!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!YYYYYYYYYYYYYY!~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~YYYYYYYYYYYYYYYJ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~JYYYYYYYYYYYYYYJ~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~JYYYYYYYYYYYYYYYY?~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~?YYYYYYYYYYYYYYYYJ~~~~~~~~~~~<br />
                            ~~~~~~~~~~?YYYYYYYYYYYYYYYYYY7~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~7YYYYYYYYYYYYYYYYYY?~~~~~~~~~~<br />
                            ~~~~~~~~~7YYYYYYYYYYYYYYYYYYYY~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~YYYYYYYYYYYYYYYYYYYY7~~~~~~~~~<br />
                            ~~~~~~~~!YYYYYYYYYYYYYYYYYYYYY?~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~?YYYYYYYYYYYYYYYYYYYYY7~~~~~~~~<br />
                            ~~~~~~~!YYYYYYYYYYYYYYYYYYYYYYY7~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~7YYYYYYYYYYYYYYYYYYYYYYY!~~~~~~~<br />
                            ~~~~~~~JYYYYYYYYYYYYYYYYYYYYYYYY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!YYYYYYYYYYYYYYYYYYYYYYYYJ~~~~~~~<br />
                            ~~~~~~JYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJ~~~~~~<br />
                            ~~~~~?YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY?~~~~~<br />
                            ~~~~7YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY7~~~~<br />
                            ~~~!???JJJJYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJJJJ???~~~~<br />
                            ~~~7?????????JJJYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJJ?????????7~~~<br />
                            ~~~??????????????JJYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJJ??????????????~~~<br />
                            ~~7??????????????????JYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJ??????????????????!~~<br />
                            ~~7?????????????????????JYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJJ????????????????????7~~<br />
                            ~~~???????????????????????JJYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJJ???????????????????????!~~<br />
                            ~~~7?????????????????????????JJYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJJ?????????????????????????7~~~<br />
                            ~~~!????????????????????????????JJYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYJJ????????????????????????????!~~~<br />
                            ~~~~!??????????????????????????????JYYYYYYYYYYYYYYYYYYYYYYYYYYYYJ??????????????????????????????!~~~~<br />
                            ~~~~~!????????????????????????????????JYYYYYYYYYYYYYYYYYYYYYYJ????????????????????????????????!~~~~~<br />
                            ~~~~~~~7?????????????????????????????????JYYYYYYYYYYYYYYYYJJ?????????????????????????????????!~~~~~~<br />
                            ~~~~~~~~!??????????????????????????????????JJYYYYYYYYYYJJ??????????????????????????????????!~~~~~~~~<br />
                            ~~~~~~~~~~!7??????????????????????????????????JJYYYYJJ??????????????????????????????????7!~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~!7???????????????????????????????????J??????????????????????????????????77~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~!7?????????????????????????????7!!!!7??????????????????????????????7!~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~77???????????????????????7!!!!!!!!!77????????????????????????7!~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~!7??????????????????77!!!!!!!!!!!!!!77??????????????????7!~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~!7????????????77!!!!!!!!!!!!!!!!!!!!7?????????????7!~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~!7???????7!!!!!!!!!!!!!!!!!!!!!!!!!!7???????7!~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!7?7!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!7?7!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!!!!!!!!!!!!!!!!!!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!!!!!!!!!!!!!!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!!!!!!!!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br />
                        </pre>
                    </a>
                </div>
                <div>
                    <a href="https://github.com/victorlohezic" className={styles.alogo} target="_blank">
                        <pre className={styles.logo}>
                            JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ??????JJYY55PPPGGGGGGPPP55YYJJ??????JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJJJJJJJJJJJJJJ???JJY5GB##&@@@@@@@@@@@@@@@@@@@@&##BG5YJJ???JJJJJJJJJJJJJJJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJJJJJJJJJJ???JYPB#&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&#BPYJ???JJJJJJJJJJJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJJJJJJJ??JYP#&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&#PYJ??JJJJJJJJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJJJJ??J5B&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&B5J??JJJJJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJ??J5B&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&B5J??JJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJ??JP#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#PJ??JJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJ??YG&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&GY??JJJJJJJJJJJ<br />
                            JJJJJJJJJJ?JG@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@GJ?JJJJJJJJJJ<br />
                            JJJJJJJJ?JP&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&PJ?JJJJJJJJ<br />
                            JJJJJJJ?Y#@@@@@@@@@@@@@B55PGB#&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&#BP555#@@@@@@@@@@@@@#Y?JJJJJJJ<br />
                            JJJJJ??P@@@@@@@@@@@@@@&J?????JJYPB&@@@@@@@@@@@&@&&&&@&@@@@@@@@@@@&BPYJJ?????J&@@@@@@@@@@@@@@P??JJJJJ<br />
                            JJJJ?JB@@@@@@@@@@@@@@@G?JJJJJJ????JYG#BGP55YYYJJJJJJJJYYY55PGB#GYJ????JJJJJJ?G@@@@@@@@@@@@@@@BJ?JJJJ<br />
                            JJJ?JB@@@@@@@@@@@@@@@@5?JJJJJJJJJJJ??????????????JJ??????????????JJJJJJJJJJJ?5@@@@@@@@@@@@@@@@BJ?JJJ<br />
                            JJ?J#@@@@@@@@@@@@@@@@@5?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?5@@@@@@@@@@@@@@@@@#J?JJ<br />
                            JJJB@@@@@@@@@@@@@@@@@@B?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?B@@@@@@@@@@@@@@@@@@BJJJ<br />
                            J?G@@@@@@@@@@@@@@@@@@@&J?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?J&@@@@@@@@@@@@@@@@@@@G?J<br />
                            ?Y@@@@@@@@@@@@@@@@@@@BJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJB@@@@@@@@@@@@@@@@@@@Y?<br />
                            ?B@@@@@@@@@@@@@@@@@@P??JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ??P@@@@@@@@@@@@@@@@@@B?<br />
                            Y@@@@@@@@@@@@@@@@@@P?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?P@@@@@@@@@@@@@@@@@@Y<br />
                            G@@@@@@@@@@@@@@@@@#JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ#@@@@@@@@@@@@@@@@@G<br />
                            &@@@@@@@@@@@@@@@@@P?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?P@@@@@@@@@@@@@@@@@&<br />
                            @@@@@@@@@@@@@@@@@@Y?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?Y@@@@@@@@@@@@@@@@@@<br />
                            @@@@@@@@@@@@@@@@@@Y?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?Y@@@@@@@@@@@@@@@@@@<br />
                            @@@@@@@@@@@@@@@@@@P?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?P@@@@@@@@@@@@@@@@@@<br />
                            @@@@@@@@@@@@@@@@@@B?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?B@@@@@@@@@@@@@@@@@@<br />
                            &@@@@@@@@@@@@@@@@@&J?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?J&@@@@@@@@@@@@@@@@@&<br />
                            B@@@@@@@@@@@@@@@@@@G?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?G@@@@@@@@@@@@@@@@@@B<br />
                            5@@@@@@@@@@@@@@@@@@@P?JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ?P@@@@@@@@@@@@@@@@@@@5<br />
                            J#@@@@@@@@@@@@@@@@@@@P??JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ??P@@@@@@@@@@@@@@@@@@@#J<br />
                            ?5@@@@@@@@@@@@@@@@@@@@BY??JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ??YB@@@@@@@@@@@@@@@@@@@@5?<br />
                            J?G@@@@@@@@@@@@@@@@@@@@@BYJ???JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ???JYB@@@@@@@@@@@@@@@@@@@@@G?J<br />
                            JJJ#@@@@@@@@@@@@@@@@@@@@@@#G5J??????JJJJJJJJJJJJJJJJJJJJJJJJJJJJ?????JJ5G#@@@@@@@@@@@@@@@@@@@@@@#JJJ<br />
                            JJ?J&@@@@@@@@B555GB&@@@@@@@@@&#GP5YJJJ??JJJJJJJJJJJJJJJJJJJJ??JJJY5PB#&@@@@@@@@@@@@@@@@@@@@@@@@&J?JJ<br />
                            JJJ?Y#@@@@@@@@G5J??J5B@@@@@@@@@@@@@&&##B5?JJJJJJJJJJJJJJJJJ5B##&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#Y?JJJ<br />
                            JJJJ?J#@@@@@@@@@&GJ???5&@@@@@@@@@@@@@@@PJJJJJJJJJJJJJJJJJJJJP@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#J?JJJJ<br />
                            JJJJJ?JG@@@@@@@@@@&5?J?JB@@@@@@@@@@@@@P?JJJJJJJJJJJJJJJJJJJJ?P@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@GJ?JJJJJ<br />
                            JJJJJJJ?5&@@@@@@@@@@5?J??YG#&@@@@@@&&GJJJJJJJJJJJJJJJJJJJJJJJJ#@@@@@@@@@@@@@@@@@@@@@@@@@@@&5?JJJJJJJ<br />
                            JJJJJJJJ?JG@@@@@@@@@@5??J??JJY55YYYJJ?JJJJJJJJJJJJJJJJJJJJJJJ?B@@@@@@@@@@@@@@@@@@@@@@@@@@GJ?JJJJJJJJ<br />
                            JJJJJJJJJ??YB@@@@@@@@@GY??????????????JJJJJJJJJJJJJJJJJJJJJJJ?B@@@@@@@@@@@@@@@@@@@@@@@@BY??JJJJJJJJJ<br />
                            JJJJJJJJJJJ??YB@@@@@@@@@#G5YYYJJJJJJYYJJJJJJJJJJJJJJJJJJJJJJJ?B@@@@@@@@@@@@@@@@@@@@@@BY??JJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJ??YG&@@@@@@@@@@@@&&&&&&@B?JJJJJJJJJJJJJJJJJJJJJJ?B@@@@@@@@@@@@@@@@@@@&GY??JJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJ??JP#@@@@@@@@@@@@@@@@@B?JJJJJJJJJJJJJJJJJJJJJJ?B@@@@@@@@@@@@@@@@@#PJ??JJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJJJ???YP#@@@@@@@@@@@@@@B?JJJJJJJJJJJJJJJJJJJJJJ?B@@@@@@@@@@@@@@#PY???JJJJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJJJJJJ???YPB&@@@@@@@@@@#?JJJJJJJJJJJJJJJJJJJJJJ?#@@@@@@@@@@&BPY???JJJJJJJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJJJJJJJJJ???JYPG#&@@@@@G?JJJJJJJJJJJJJJJJJJJJJJ?G@@@@@&#GPYJ???JJJJJJJJJJJJJJJJJJJJJJJ<br />
                            JJJJJJJJJJJJJJJJJJJJJJJJJJJ????JY5GG5JJJJJJJJJJJJJJJJJJJJJJJJJJ5GG5YJ????JJJJJJJJJJJJJJJJJJJJJJJJJJJ<br />
                        </pre>
                    </a>
                </div>
            </div>
            <Cmdline next_path="contact" cmd="echo $mail" />
            <a href="mailto: vlohezic@enseirb-matmeca.fr" id={styles.mail}>vlohezic@enseirb-matmeca.fr</a>
        </Layout>
    );
}
